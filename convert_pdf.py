from PIL import Image
import pytesseract
from wand.image import Image as Img
import os

BASE_DIR = os.getcwd()
INPUT_DIR = os.path.join(BASE_DIR, 'input')
IMAGES_DIR = os.path.join(BASE_DIR, 'images')
OUTPUT_DIR = os.path.join(BASE_DIR, 'output')


def get_all_pdfs():
    for item in os.listdir(INPUT_DIR):
        if all([
            not item.startswith('.'),
            item.endswith('.pdf'),
            os.path.isfile(f'{INPUT_DIR}/{item}'),
        ]):
            yield item


def convert_pdf_to_image(pdfs):
    if not os.path.exists(IMAGES_DIR):
        os.mkdir(IMAGES_DIR)
    for pdf in pdfs:
        with Img(filename=f'{INPUT_DIR}/{pdf}', resolution=300) as img:
            # img.compression_quality = 99
            img.save(filename='images/ocr_me.jpg')


def get_all_imgs():
    for item in os.listdir(IMAGES_DIR):
        if all([
            not item.startswith('.'),
            item.endswith('.jpg'),
            os.path.isfile(f'{IMAGES_DIR}/{item}'),
        ]):
            yield item


def convert_img_to_txt(images):
    if not os.path.exists(OUTPUT_DIR):
        os.mkdir(OUTPUT_DIR)
    for image in images:
        the_ocr_text = pytesseract.image_to_string(Image.open(f'{IMAGES_DIR}/{image}'))
        new_filename = os.path.splitext(image)[0] + '.txt'
        open(os.path.join(OUTPUT_DIR, new_filename), 'w').write(the_ocr_text)


def main():
    convert_pdf_to_image(get_all_pdfs())
    convert_img_to_txt(get_all_imgs())


if __name__ == '__main__':
    main()
